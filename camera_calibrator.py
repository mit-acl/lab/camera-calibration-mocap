# -*- coding: utf-8 -*-
"""
Finds the transformation from the mocap body to the camera

Created on Sun Dec 11 17:19:57 2022

@author: Dominic
@maintainer: Mason Peterson, Parker Lusk
"""

import cv2
import numpy as np
import gtsam
from pupil_apriltags import Detector
import rosbag
from cv_bridge import CvBridge
from scipy.spatial.transform import Rotation as Rot

# TODO: use robot_data objects to generalize for other data formats

class CameraCalibrator():
    
    def __init__(self, K, D=np.zeros(4), fisheye=False):
        self.K = K
        self.D = D
        self.fisheye = fisheye
        fx = K[0,0]; fy = K[1,1]; cx = K[0,2]; cy = K[1,2]
        self.intrinsics = ([fx, fy, cx, cy])

    def track_tags(self, img, at_detector, intrinsics, tag_size, visualize=True, resize=1):
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        tags = at_detector.detect(img_gray, estimate_tag_pose=True,
                                camera_params=intrinsics, tag_size=tag_size)
        
        # assume there is just one tag
        if len(tags) == 0:
            if visualize:
                cv2.imshow("drone cam", img)
            return
        tag = tags[0]
        
        if visualize:
            # extract the bounding box (x, y)-coordinates for the AprilTag
            # and convert each of the (x, y)-coordinate pairs to integers
            (ptA, ptB, ptC, ptD) = tag.corners
            ptB = (int(ptB[0]), int(ptB[1]))
            ptC = (int(ptC[0]), int(ptC[1]))
            ptD = (int(ptD[0]), int(ptD[1]))
            ptA = (int(ptA[0]), int(ptA[1]))
            # draw the bounding box of the AprilTag detection
            cv2.line(img, ptA, ptB, (0, 255, 0), 2)
            cv2.line(img, ptB, ptC, (0, 255, 0), 2)
            cv2.line(img, ptC, ptD, (0, 255, 0), 2)
            cv2.line(img, ptD, ptA, (0, 255, 0), 2)
            # draw the center (x, y)-coordinates of the AprilTag
            (cX, cY) = (int(tag.center[0]), int(tag.center[1]))
            cv2.circle(img, (cX, cY), 5, (0, 0, 255), -1)
            # draw the tag family on the image
            tagFamily = tag.tag_family.decode("utf-8")
            cv2.putText(img, tagFamily, (ptA[0], ptA[1] - 15),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                
            height, width, channels = img.shape
            img = cv2.resize(img, (int(width*resize), int(height*resize)))
            
            cv2.imshow("drone cam", img)
            cv2.waitKey(0)
            # return tag, img
        
        return tag

    def find_T_body_camera(self, bagfiles, camera_topic, tag_pose_topic, camera_pose_topic, tag_size, visualize=False, debug=False, T_tag_fix=np.eye(4)):
        # initialize detector
        at_detector = Detector(families='tag36h11',
                        nthreads=1,
                        quad_decimate=1.0,
                        quad_sigma=0.0,
                        refine_edges=1,
                        decode_sharpening=0.25,
                        debug=0)
        
        T_body_camera_list = []
        for bagfile in bagfiles:

            got_new_image = False
            got_new_tag_pose = False
            got_new_camera_pose = False
            T_body_camera = None

            for topic, msg, t in rosbag.Bag(bagfile).read_messages(topics=[camera_topic, tag_pose_topic, f"/{tag_pose_topic}", camera_pose_topic]):
                if not got_new_image and topic == camera_topic:
                    bridge = CvBridge()
                    cv_image = bridge.compressed_imgmsg_to_cv2(msg)
                    cv_image.astype(np.uint8)
                    cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
                    if self.fisheye:
                        cv_image = cv2.fisheye.undistortImage(cv_image, self.K, self.D, None, self.K)
                    else:
                        cv_image = cv2.undistort(cv_image, self.K, self.D, None, self.K)
                    got_new_image = True
                    if debug: print("Got image")

                if not got_new_tag_pose and (topic == f"/{tag_pose_topic}" or topic == tag_pose_topic):
                    position = msg.pose.position
                    quat = msg.pose.orientation
                    T_world_tag = gtsam.Pose3(gtsam.Rot3(quat.w, quat.x, quat.y, quat.z), gtsam.Point3(position.x, position.y, position.z))
                    T_world_tag = T_world_tag.matrix() @ T_tag_fix
                    got_new_tag_pose = True
                    if debug: print("Got tag pose")

                if not got_new_camera_pose and topic == camera_pose_topic:
                    position = msg.pose.position
                    quat = msg.pose.orientation
                    T_world_body = gtsam.Pose3(gtsam.Rot3(quat.w, quat.x, quat.y, quat.z), gtsam.Point3(position.x, position.y, position.z))
                    got_new_camera_pose = True
                    if debug: print("got camera/body")
                
            if got_new_image and got_new_tag_pose and got_new_camera_pose:
                # track tags if image and vicon data received
                if debug: print("trying detecting tag")
                tag = self.track_tags(cv_image, at_detector, self.intrinsics, tag_size, visualize=visualize)
                if tag:
                    T_camera_tag = gtsam.Pose3(gtsam.Rot3(tag.pose_R), gtsam.Point3(tag.pose_t[0,0], tag.pose_t[1,0], tag.pose_t[2,0]))
                    T_body_camera = (T_world_body.inverse().matrix()) @ T_world_tag @ (T_camera_tag.inverse().matrix())
                else:
                    T_camera_tag = T_body_camera = None

                    # what we actually care about, the transform of the camera frame wrt the body frame
            if debug: print(T_body_camera)
            if T_body_camera is not None:
                T_body_camera_list.append(np.copy(T_body_camera))
                rot = Rot.from_matrix(T_body_camera[:3,:3]).as_euler('xyz', degrees=True)
                if debug: print()
                if debug: print(rot)
                if debug: print()

        # Find mean
        rotations = []
        translations = []
        for T in T_body_camera_list:
            rotations.append(T[:3, :3])
            translations.append(T[:3,3].reshape((3,1)))
        R_mean = Rot.mean(Rot.from_matrix(np.array(rotations))).as_matrix()
        t_mean = np.mean(translations, axis=0)
        T_body_camera_mean = np.concatenate([
            np.concatenate([R_mean, t_mean], axis=1), 
            [[0, 0, 0, 1]]
            ], axis=0)
        if debug: print(T_body_camera_mean)
        if debug:
            print(f'translation standard deviation: {np.std(translations, axis=0).reshape(-1)}')
        return T_body_camera_mean