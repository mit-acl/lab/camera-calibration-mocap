Camera Calibration Mocap
========================

You need a ros bag recorded with:

- tag box vicon (world)
- the camera image
- probably want camera info
- "the thing" that the camera is attached to

the output:

the extrinsic calibration between "the thing" and the camera

tip:

- record many bag files, each a second or two and completely stationary
- then, just use wildcard in `example_calib.py` for `--bagfile` and it will automatically average multiple calibration instances to give a better extrinsic.

## Install

`clone` and `cd` into this (camera-calibration-mocap) directory and `git submodule update --init` to pull submodules.

Run:

```
pip install -r ./requirements.txt
```