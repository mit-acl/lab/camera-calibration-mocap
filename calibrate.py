from robot_utils.robot_data.img_data import ImgData
from camera_calibrator import CameraCalibrator
import gtsam
import numpy as np

def calibrate(
    bagfiles,
    topic_body,
    topic_tag,
    topic_img,
    topic_caminfo,
    fisheye=False,
    visualize=False,
    debug=False
):
    
    img_data = ImgData(bagfiles[0], file_type='bag', topic=topic_img)
    img_data.extract_params(topic_caminfo)
    tag_size = 0.1655

    #################################################################################

    T_tag_fix = gtsam.Pose3(gtsam.Rot3.Rz(-np.pi/2),np.array([0,0,0])).matrix()
    cam_calib_mocap = CameraCalibrator(K=img_data.K, D=img_data.D, fisheye=fisheye)
    T_body_camera = cam_calib_mocap.find_T_body_camera(bagfiles, topic_img, 
        topic_tag, topic_body, tag_size=tag_size, T_tag_fix=T_tag_fix, visualize=visualize, 
        debug=debug)
    
    return T_body_camera
    
    

if __name__ == '__main__':
    ################################# params ######################################
    # location of rosbag which contains images, vicon location of tag, vicon location of box
    # Note that this code assumes the vicon body frame of the tag has origin at the tag center, x to the right, y down, and z into the tag

    import argparse
    import glob
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bagfile', required=True, help='Path to bagfile(s), can use wildcards')
    parser.add_argument('-p', '--pose-topic', required=True, help='Body name in ROS topic: RR01')
    parser.add_argument('-c', '--camera-prefix', default='t265', help='Camera name in ROS topic, ex: t265, d435')
    parser.add_argument('-v', '--visualize', action='store_true')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('--compressed', action='store_true')
    args = parser.parse_args()

    camera_prefix = f"{'/' if args.camera_prefix[0] != '/' else ''}{args.camera_prefix}{'/' if args.camera_prefix[-1] != '/' else ''}"
    debug = args.debug
    compressed = args.compressed
    visualize = args.visualize
    bagfiles = []
    if debug: print('debug log')
    
    for filename in glob.glob(args.bagfile):
        if debug: print(f'Opening bag: {filename}')
        bagfiles.append(filename)

    topic_tag = "/tag_box/world"
    topic_pose = args.pose_topic
    topic_camera = f"{camera_prefix}image_raw{'/compressed' if compressed else ''}"
    topic_caminfo = f"{camera_prefix}camera_info"
    
    T_body_cam = calibrate(
        bagfiles=bagfiles,
        topic_body=topic_pose,
        topic_tag=topic_tag,
        topic_img=topic_camera,
        topic_caminfo=topic_caminfo,
        fisheye=True if 't265' in camera_prefix else False,
        visualize=visualize,
        debug=debug
    )
    print('T_body_camera:')
    print(T_body_cam)
    print(T_body_cam.reshape(-1).tolist())
